import { Component } from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {

    columnDefs = [
        { field: 'NAME' },
        { field: 'AGE' },
        { field: 'ID'}
    ];

    rowData = [
        { NAME: 'Naveen', AGE: '20', ID: 11 },
        { NAME: 'Raj', AGE: '21', ID: 22 },
        { NAME: 'NaveenRaj', AGE: '22', ID: 33 }
    ];
}